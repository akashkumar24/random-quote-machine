function QuoteBox() {
  const [quote, setQuote] = React.useState("");
  const [author, setAuthor] = React.useState("");
  const [color, setColor] = React.useState("#fff");

  const fetchNewQuote = async () => {
    const response = await fetch("https://type.fit/api/quotes");
    const data = await response.json();

    const randomIndex = Math.floor(Math.random() * data.length);
    const randomQuote = data[randomIndex];
    setQuote(randomQuote.text);
    setAuthor(randomQuote.author || "Unknown");
    setColor(getRandomColor());
  };

  const getRandomColor = () => {
    const colors = [
      "#16a085",
      "#27ae60",
      "#2c3e50",
      "#f39c12",
      "#e74c3c",
      "#9b59b6",
      "#FB6964",
      "#342224",
      "#472E32",
      "#BDBB99",
      "#77B1A9",
      "#73A857",
    ];
    return colors[Math.floor(Math.random() * colors.length)];
  };

  React.useEffect(() => {
    fetchNewQuote();
  }, []);

  const tweetUrl = `https://twitter.com/intent/tweet?text=${encodeURIComponent(
    `"${quote}" - ${author}`
  )}`;
  const buttonStyles = {
    backgroundColor: color,
    borderColor: color,
  };

  return (
    <div id="quote-box" style={{ backgroundColor: color }}>
      <div className="container" style={{ maxWidth: "600px" }}>
        <div className="card">
          <div className="card-header">Inspirational Quotes</div>
          <div className="card-body">
            <p id="text">&quot;{quote}</p>
            <div className="author-container">
              <p id="author">- {author}</p>
            </div>

            <div className="button-container">
              <div style={{ display: "flex" }}>
                <a
                  id="tweet-quote"
                  href={tweetUrl}
                  target="_blank"
                  rel="noopener noreferrer"
                  className="btn btn-warning"
                  style={{ buttonStyles }}
                >
                  <i className="fa fa-twitter" style={{ color: "white" }}></i>
                </a>
                <a
                  href={"https://www.tumblr.com/"}
                  target="_blank"
                  className="btn btn-danger"
                  style={{ buttonStyles }}
                >
                  <i className="fa fa-tumblr"></i>
                </a>
              </div>
              <button
                id="new-quote"
                onClick={fetchNewQuote}
                className="btn btn-primary ml-3"
                style={{ buttonStyles }}
              >
                New quote
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

function App() {
  return (
    <div>
      <QuoteBox />
    </div>
  );
}

ReactDOM.render(<App />, document.getElementById("app"));
